public class QueueApp {
    public static void main(String[] args) {
        Queue theQueue = new Queue(5);
        theQueue.insert(10); // insert 4 items
        theQueue.insert(20);
        theQueue.insert(30);
        theQueue.insert(40);
        theQueue.insert(110); // insert 4 items
        theQueue.insert(220);
        theQueue.insert(330);
        theQueue.insert(440);

        System.out.println("size = " + theQueue.size());

        while (!theQueue.isEmpty()) {
            long n = theQueue.remove(); // (220, 330, 440, 40, 110)
            System.out.print(n);
            System.out.print(" ");
        }
        System.out.println("จบ");
        System.out.println(theQueue.peekFront());
        System.out.println("size = " + theQueue.size());

    }
}
